describe('Testing welcome page', () => {
  it('should toggle text', () => {
    cy.visit('http://localhost:5000/welcome');
    cy.get('#toggle-btn').click();
    cy.get('#hidden-text').should('be.visible');
  })
})